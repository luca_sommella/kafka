package producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

public class ProducerThread implements Runnable {
    private AtomicLong value ;
    private Properties properties;
    private String topicName;
    
    public ProducerThread(AtomicLong value, Properties properties, String topicName) {
        super();
        this.value = value;
        this.properties = properties;
        this.topicName = topicName;
    }
    

    public void run() 
    { 
        try
        { 

            String eventMessage = "";
            String codice = "LUKA-" + value;
            Date tempo = new Date();
            System.out.println("Run producer for message with correlation id "+ codice);
            
            eventMessage = "Simple Kafka message sent at "+ tempo.getTime();


            
            Producer<String, String> producer = new KafkaProducer <String, String>(properties);
            
            
            
            Future<RecordMetadata> send = producer.send(new ProducerRecord<String, String>(topicName, 
                    null, eventMessage));
            Date d = new Date();
            d.setTime(send.get().timestamp());
            System.out.println("#######################################################################################");
            System.out.println("In date " + d);
            System.out.println("Evento: " + eventMessage);
            System.out.println("Send message on "+send.get().topic());
            System.out.println("At partition " + send.get().partition());
            System.out.println("At offset " + send.get().offset());
            System.out.println("#######################################################################################");
            
            
            producer.close();

        } 
        catch (Exception e) 
        { 
            // Throwing an exception 
            System.out.println ("Exception is caught"); 
        } 
    } 
} 