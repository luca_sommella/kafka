package producer;

import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

public class ProducerKafka {
    
    
    public static void main(String[] args) {
        PropertiesManager p = new PropertiesManager();
        

        dammiUnoStream(p.getNumberEvents(), p.getStartFrom()).forEach(i -> {
            Thread object = new Thread(new ProducerThread(new AtomicLong(i), p.getKafkaProperties(), p.getTopicName())); 
            object.start();
        });
        
        

    }
    
    private static IntStream dammiUnoStream (Integer numeroColpi, Integer partiDa) {
        return IntStream.range(partiDa, partiDa+numeroColpi).parallel();
    }
}
