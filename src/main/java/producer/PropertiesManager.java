package producer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesManager {
    private Properties kafkaProperties;
    
    private Properties properties;

    public Properties getKafkaProperties() {

        return this.kafkaProperties;
    }
    
    public Properties getProperties() {

        return this.properties;
    }

    public String getTopicName() {
        return this.kafkaProperties.getProperty("kafka.topic.name");
    }
    
    public Integer getNumberEvents() {
        return this.properties.getProperty("number.events") != null
                ? Integer.parseInt(this.properties.getProperty("number.events"))
                        : 1;
    }
    
    public Integer getStartFrom() {
        return this.properties.getProperty("parti.da") != null
                ? Integer.parseInt(this.properties.getProperty("parti.da"))
                        : 0;
    }
    
    public String getPrefissoCorrelationId() {
        return this.properties.getProperty("prefisso.correlationId")!=null
                ? this.properties.getProperty("prefisso.correlationId")
                        : "Correlation-Id-";
    }

    public PropertiesManager() {
        super();
        this.properties = new Properties();
        try (InputStream input = new FileInputStream("kafka.properties")) {


            properties.load(input);

        } catch (IOException ex) {
            System.out.println("Errore durante la lettura delle proprieties per kafka client. ");
            System.out.println("Saranno usate le properties di default. ");
            ex.printStackTrace();
        } 

        this.kafkaProperties = new Properties();
        try (InputStream input = new FileInputStream("kafka.properties")) {


            kafkaProperties.load(input);

        } catch (IOException ex) {
            System.out.println("Errore durante la lettura delle proprieties. ");
            System.out.println("Saranno usate le properties di default. ");
            ex.printStackTrace();
        }
            
        System.out.println("################################ KAFKA PROPERTIES #####################################");
        kafkaProperties.forEach( (k,v) -> {
            System.out.println(k + ": " + v);
        });
        System.out.println("#######################################################################################");
        
        System.out.println("################################ EVENTS PROPERTIES #####################################");
        properties.forEach( (k, v) -> {
            System.out.println(k + ": " + v);
        });
        System.out.println("#######################################################################################");

    }
}
